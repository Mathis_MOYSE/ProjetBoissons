package controller;

import junit.framework.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import Controller.MachineController;
import bddutil.Hibernate;

public final class MachineController_test{

	MachineController machineController = new MachineController();

	@BeforeEach
	void SetUp() throws Exception {

	}

	@Test
	public void test_MachineInitialisazion_is_false_when_does_not_exit(){
		Assert.assertFalse(machineController.MachineInitialisazion(8));
	}

	@Test
	public void test_MachineInitialisazion_is_OK(){
		Assert.assertTrue(machineController.MachineInitialisazion(5));
	}

	@Test
	public void test_ControllerWaterQuantity_when_quantity_down_35cl(){
		machineController = new MachineController(34,1000);
		Assert.assertEquals(machineController.ControllerWaterQuantity(),2);
	}
	
	@Test
	public void  test_ControllerWaterQuantity_when_quantity_upper_75cl() {
		machineController = new MachineController(75,1000);
		Assert.assertEquals(machineController.ControllerWaterQuantity(),0);
	}
	
	@Test
	public void test_ControllerWaterQuantity_when_quantity_down_75cl() {
		machineController = new MachineController(74,1000);
		Assert.assertEquals(machineController.ControllerWaterQuantity(),1);
	}
	
	@Test 
	public void test_ControllerSugarQuantity_when_quantity_upper_to_25gr() {
		machineController = new MachineController(25,25);
		Assert.assertEquals(machineController.ControllerSugarQuantity(),1);
	}
	
	@Test 
	public void test_ControllerSugarQuantity_when_quantity_equal_or_upper_20gr() {
		machineController = new MachineController(20,20);
		Assert.assertEquals(machineController.ControllerSugarQuantity(),2);
	}
	
	@Test 
	public void test_ControllerSugarQuantity_when_quantity_equal_or_upper_15gr() {
		machineController = new MachineController(15,15);
		Assert.assertEquals(machineController.ControllerSugarQuantity(),3);
	}
	
	@Test 
	public void test_ControllerSugarQuantity_when_quantity_equal_or_upper_10gr() {
		machineController = new MachineController(10,10);
		Assert.assertEquals(machineController.ControllerSugarQuantity(),4);
	}
	
	@Test 
	public void test_ControllerSugarQuantity_when_quantity_equal_or_upper_5gr() {
		machineController = new MachineController(5,5);
		Assert.assertEquals(machineController.ControllerSugarQuantity(),5);
	}
	
	@Test 
	public void test_ControllerSugarQuantity_when_quantity_equal_0gr_or_down_5gr() {
		machineController = new MachineController(34,0);
		Assert.assertEquals(machineController.ControllerSugarQuantity(),0);
	}

}