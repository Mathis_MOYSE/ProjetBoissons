package Model;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.*;
import model.Orders;

import model.Cup;

@ExtendWith(MockitoExtension.class)
/**
 * 
 * Dans cette classe de test, un object mockable pourra être la commande ajoutée à notre object cup, bien que 
 * cup ne s'appuie pas sur un retour d'une méthode de la commande mais se contente de l'ajouter
 */
class Cup_test {
	static private Orders mockedOrderToAdd = Mockito.mock(Orders.class);
	static private Cup cupSuccessExpected;
	static private Cup cupFailExpected;
	@BeforeAll
	public static void initCupsForTests() 
	{
		//stubbing the mocked order
		when(mockedOrderToAdd.getIdC()).thenReturn(1);
		doCallRealMethod().when(mockedOrderToAdd).setDateTime(Mockito.any(LocalDateTime.class));
		doCallRealMethod().when(mockedOrderToAdd).setIdC(Mockito.anyInt());
		
		cupSuccessExpected = new Cup();
		cupSuccessExpected.setIdCup(1);
		cupSuccessExpected.setSize(35);
		cupSuccessExpected.setQuantity(20);
		
		cupFailExpected = new Cup();
		cupFailExpected.setIdCup(2);
		cupFailExpected.setSize(35);
		cupFailExpected.setQuantity(0);
	}
	@Test
	public void testTakeQuantityFailed() 
	{
		assertThrows(RuntimeException.class, () -> {
			cupFailExpected.TakeCup();
		});
	}
	@Test
	public void testTakeQuantityNormal() {
		
		cupSuccessExpected.TakeCup();	
		assertEquals(19, cupSuccessExpected.getQuantity());
	}
	
	@Test
	public void testBadQuantity()
	{
		Cup cup = new Cup(-100, 35, (float)0.2);
		assertEquals(0, cup.getQuantity());
		
	}
	@Test
	public void testAddOrder() {
		cupSuccessExpected.addOrder(mockedOrderToAdd);
		assertEquals(1, cupSuccessExpected.getOrderedList().size());
	}

	@Test
	public void testAddNullOrder()
	{
		Cup cup = new Cup(2, 35, 1);
		cup.addOrder(null);
		assertEquals(0, cup.getOrderedList().size());
	}
}