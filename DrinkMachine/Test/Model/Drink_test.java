package Model;
import static org.junit.jupiter.api.Assertions.assertEquals;

import model.Orders;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.Mock;

import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.MockitoAnnotations;
import model.Drink;
import model.Machine;
/**
 * on crée des mocks pour les objets Machine et Order car on ne cherche pas à tester leur comportement
 * mais nous en avons cependant besoin dans le cadre de nos tests
 *
 */
@ExtendWith(MockitoExtension.class)
public class Drink_test {
	
	@Mock Machine machine1;
	@Test
	public void test_Add_Machine() {
		Drink drink1 = new Drink();
		drink1.addMachine(machine1);
		assertEquals(1,drink1.getMachineBelongList().size());
	}
	@Test
	public void test_Remove_Machine() 
	{
		Drink drink1 = new Drink();
		drink1.getMachineBelongList().add(machine1);
		drink1.removeMachine(machine1);
		assertEquals(0, drink1.getMachineBelongList().size());
	}
	@Mock Orders order1;
	@Test
	public void test_Add_Orders() {
		Drink drink1 = new Drink();
		drink1.addOrder(order1);
		assertEquals(1,drink1.getOrdersList().size());
	}
	@Test
	public void test_Remove_Orders() 
	{
		Drink drink1 = new Drink();
		drink1.getOrdersList().add(order1);
		drink1.removeOrder(order1);
		assertEquals(0,drink1.getOrdersList().size());
	}
}
