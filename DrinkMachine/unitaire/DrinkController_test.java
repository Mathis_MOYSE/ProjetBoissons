package unitaire;
import static org.junit.jupiter.api.Assertions.assertEquals;
import junit.framework.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import Controller.DrinkController;
import model.Cup;
import model.Drink;
import model.Machine;
import model.Orders;

public class DrinkController_test { //NE PASSE PAS
	
	@Test
	public void test_Find_Drink_By_Id() {
		Drink drink1 = new Drink();
		drink1.setDescription("good drink");
		drink1.setNom("coca cola zero");
		drink1.setPrixCl(20);
		
		DrinkController drink1_dao = new DrinkController();
		drink1_dao.addDrink(drink1);
		assertEquals(20,drink1_dao.getDrink(drink1.getIdBoisson()).getPrixCl());
		
		drink1_dao.DeleteDrink(drink1.getIdBoisson());
	}

	@Test
	public void test_Modif_Drink() { 
		Drink drink1 = new Drink();
		drink1.setDescription("good drink");
		drink1.setNom("coca cola zero");
		drink1.setPrixCl(20);
		
		DrinkController drink1_dao = new DrinkController();
		drink1_dao.addDrink(drink1);
		
		drink1.setPrixCl((float) 3);  // modification de drink sur controlleur
		
		drink1_dao.ModifDrink(drink1);
		assertEquals(3,drink1_dao.getDrink(drink1.getIdBoisson()).getPrixCl());
		
		drink1_dao.DeleteDrink(drink1.getIdBoisson());
	}
	
	@Test
	public void test_display_all_drinks() { 
		Drink drink1 = new Drink();
		drink1.setNom("coca cola zero");
		drink1.setDescription("good drink");
		drink1.setPrixCl(8);
		
		Drink drink2 = new Drink();
		drink2.setNom("coffe");
		drink2.setDescription("good coffe");
		drink2.setPrixCl(5);
		
		DrinkController drink1_dao = new DrinkController();
		drink1_dao.addDrink(drink1);
		drink1_dao.addDrink(drink2);
		assertEquals(2,drink1_dao.getAllDrinks().size());
		
		drink1_dao.DeleteDrink(drink1.getIdBoisson());
		drink1_dao.DeleteDrink(drink2.getIdBoisson());

	}
}
