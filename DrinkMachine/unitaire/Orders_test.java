package unitaire; 
 
import static org.junit.jupiter.api.Assertions.assertEquals; 
 
import java.text.DecimalFormat; 
 
import org.junit.jupiter.api.BeforeEach; 
import org.junit.jupiter.api.Test; 
 
import model.Cup; 
import model.Drink; 
import model.Orders; 
 
 
public class Orders_test { 
	 
	 
	Orders o1; 
	Cup c1; 
	 
	@BeforeEach 
	public void SetUp() { 
		Drink dr1 = new Drink(); 
	    dr1.setPrixCl((float)0.5); 
 
        Cup c1 = new Cup(); 
        c1.setQuantity(100); 
        c1.setSize(35); 
        c1.setPrice((float)0.10); 
         
        Orders o1 = new Orders(); 
        o1.setOrderedDrink(dr1); 
        o1.setQuantity(35); 
	} 
	 
	@Test 
	public void CalculatePrice_with_cup() { 
		o1.setOrderedCup(c1); 
		o1.CalculatePrice(); 
		DecimalFormat f = new DecimalFormat(); 
	    f.setMaximumFractionDigits(1); 
	    assertEquals(f.format(17.6),f.format(o1.getPrice())); 
	} 
	 
	@Test 
	public void CalculatePrice_with_no_cup() { 
		o1.setOrderedCup(null); 
		o1.CalculatePrice(); 
		DecimalFormat f = new DecimalFormat(); 
	    f.setMaximumFractionDigits(1); 
	    assertEquals(f.format(17.5),f.format(o1.getPrice())); 
	} 
	 
	 
} 
