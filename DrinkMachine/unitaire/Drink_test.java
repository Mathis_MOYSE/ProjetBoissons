package unitaire;
import static org.junit.jupiter.api.Assertions.assertEquals;

import model.*;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;


import Controller.DrinkController;
import static org.junit.jupiter.api.Assertions.assertEquals;

import model.Orders;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.Mock;

import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.MockitoAnnotations;
import model.Drink;
import model.Machine;
/**
 * on crée des mocks pour les objets Machine et Order car on ne cherche pas à tester leur comportement
 * mais nous en avons cependant besoin dans le cadre de nos tests
 *
 */
@ExtendWith(MockitoExtension.class)
public class Drink_test {
	
	@Mock Machine machine1;
	@Test
	public void test_Add_Machine() {
		Drink drink1 = new Drink();
		drink1.addMachine(machine1);
		assertEquals(1,drink1.getMachineBelongList().size());
	}
	@Test
	public void test_Remove_Machine() 
	{
		Drink drink1 = new Drink();
		drink1.getMachineBelongList().add(machine1);
		drink1.removeMachine(machine1);
		assertEquals(0, drink1.getMachineBelongList().size());
	}
	@Mock Orders order1;
	@Test
	public void test_Add_Orders() {
		Drink drink1 = new Drink();
		drink1.addOrder(order1);
		assertEquals(1,drink1.getOrdersList().size());
	}
	@Test
	public void test_Remove_Orders() 
	{
		Drink drink1 = new Drink();
		drink1.getOrdersList().add(order1);
		drink1.removeOrder(order1);
		assertEquals(0,drink1.getOrdersList().size());
	}
	
	@Test
	public void test_Add_Remove_Machine() {
		Drink drink1 = new Drink();
		
		Machine machine1 = new Machine();
		
		machine1.setIdMachine(1);
		machine1.setSugarGr(100);
		machine1.setWaterCl(500);
		
		drink1.addMachine(machine1);
		assertEquals(1,drink1.getMachineBelongList().size());

		drink1.removeMachine(machine1);
		assertEquals(0,drink1.getMachineBelongList().size());

	}
	
	@Test
	public void test_Add_Remove_Orders() {
		Drink drink1 = new Drink();
		
		Orders order1 = new Orders();
		
		order1.setIdC(1);
		order1.setDateTime(LocalDateTime.now());
		order1.setPrice((float) 5.3);
		
		drink1.addOrder(order1);
		assertEquals(1,drink1.getOrdersList().size());
		
		drink1.removeOrder(order1);
		assertEquals(0,drink1.getOrdersList().size());
	}

	@Test
	public void Drinkcomparator_sort_list_by_Id() { 
		Drink dr1 = new Drink();
		dr1.setIdBoisson(1);
		Drink dr2 = new Drink();
		dr1.setIdBoisson(2);
		Drink dr3 = new Drink();
		dr1.setIdBoisson(3);

		ArrayList<Drink> list = new ArrayList<>();
		list.add(dr2);
		list.add(dr3);
		list.add(dr1);
		
		Collections.sort(list, new DrinkComparator());

		assertEquals(list.get(0), dr1);
		assertEquals(list.get(1), dr2);
		assertEquals(list.get(2), dr3);
	}
}
