package unitaire;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.Cup;
import model.Drink;
import model.Machine;
import model.Orders;

public class Machine_test {
	
	 Machine machine1;
	 Cup c1;
	 Orders o;
	 Drink dr1;
	
	@BeforeEach
	public void SetUp() {
		
		machine1 = new Machine();
		
		Set<Cup> cups = new HashSet<>();
		c1 = new Cup();
		c1.setQuantity(1);
		cups.add(c1);
		
		
		Set<Orders> orders = new HashSet<>();
		o = new Orders();
		orders.add(o);
		
		Set<Drink> drinks = new HashSet<>();
		dr1 = new Drink();
		drinks.add(dr1);
		
		machine1.setStockCup(cups);
		machine1.setDrinksList(drinks);
		machine1.setOrderedList(orders);
		machine1.setSugarGr(100);
		machine1.setWaterCl(100);
		
	}
	
	@Test
	public void addOrder_is_OK() {
		machine1.addOrder(new Orders());
		assertEquals(2, machine1.getOrderedList().size());
		
	}
	
	@Test
	public void addOrder_when_order_is_null() {
		machine1.addOrder(null);
		assertEquals(1, machine1.getOrderedList().size());
		
	}
	
	@Test
	public void removeOrder_is_OK() {
		machine1.removeOrder(o);
		assertEquals(0, machine1.getOrderedList().size());
		
	}
	
	@Test
	public void removeOrder_when_order_is_null() {
		machine1.removeOrder(null);
		assertEquals(1, machine1.getOrderedList().size());
	}
	
	@Test
	public void addDrink_is_OK() {
		machine1.addDrink(new Drink());
		assertEquals(2, machine1.getDrinksList().size());
	}
	
	@Test
	public void addDrink_when_drink_is_null() {
		machine1.addDrink(null);
		assertEquals(1, machine1.getDrinksList().size());
	}
	
	@Test
	public void removeDrink_is_OK() {
		machine1.removeDrink(dr1);
		assertEquals(0, machine1.getDrinksList().size());
	}
	
	@Test
	public void removeDrink_when_drink_is_null() { 
		machine1.removeDrink(dr1);
		assertEquals(1, machine1.getDrinksList().size());
	}
	
	@Test
	public void addCup_is_OK(){
		machine1.addCup(new Cup());
		assertEquals(2, machine1.getStockCup().size());
		
	}
	
	@Test
	public void addCup_when_cup_is_null() {
		machine1.addCup(null);
		assertEquals(1, machine1.getStockCup().size());
	}
	
	@Test
	public void removeCup_is_OK(){
		machine1.removeCup(c1);
		assertEquals(0, machine1.getStockCup().size());
	}
	
	@Test
	public void removeCup_when_cup_is_null() {
		machine1.removeCup(null);
		assertEquals(1, machine1.getStockCup().size());
	}
	
	@Test
	public void subtractSugar_enought_quantity() { 
		machine1.subtractSugar(10);
		assertEquals(90, machine1.getSugarGr());
	}
	
	@Test
	public void subtractSugar_not_enought_quantity() { 
		machine1.setSugarGr(5);
		machine1.subtractSugar(10);
		assertEquals(100, machine1.getSugarGr());

	}
	
	@Test
	public void subtractWater_enought_quantity() {
		machine1.subtractWater(10);
		assertEquals(90, machine1.getWaterCl());
	}
	
	@Test
	public void subtractWater_not_enought_quantity() {
		machine1.setWaterCl(10);
		machine1.subtractWater(20);
		assertEquals(10,machine1.getWaterCl());
	}
	
	@Test
	public void substractCup_enought_quantity() {
			machine1.substractCup(c1);
	}
	
	@Test
	public void substractCup_not_enought_quantity() {
		
	}
}
