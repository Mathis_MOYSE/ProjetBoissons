package unitaire;
import Controller.OrderController;
import bddutil.Hibernate;
import model.Cup;
import model.Drink;
import model.Machine;
import model.Orders;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.function.Executable;


import javax.persistence.Query;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class OrderController_test {

    private static Drink dr1;
    private Orders orders;
    private static Cup c1;
    private static Machine m1;

    private OrderController orderController;

    @BeforeAll
    public static void SetUpAll(){
        dr1 = new Drink();
        dr1.setPrixCl((float)0.5);

        c1 = new Cup();
        c1.setQuantity(100);
        c1.setSize(35);
        c1.setPrice((float)0.10);

        Set<Drink> list = new HashSet<>();
        list.add(dr1);

        Set<Cup> list2 = new HashSet<>();
        list2.add(c1);

        m1 = new Machine();
        m1.setWaterCl(1000);
        m1.setSugarGr(1000);


    }

    @BeforeEach
    public void SetUp(){
        orders = new Orders();
        orders.setOrderedDrink(dr1);
        orders.setQuantity(35);

        orderController = new OrderController();
    }

    @Test
    public void test_setPriceOrder_with_no_cup(){

        orders.setOrderedCup(null);

        orderController.setPriceOrder(orders);

        assertEquals(17.5,orders.getPrice());

    }

    @Test
    public void test_setPriceOrder_with_cup(){
        orders.setOrderedCup(c1);

        orderController.setPriceOrder(orders);

        DecimalFormat f = new DecimalFormat();
        f.setMaximumFractionDigits(1);
        assertEquals(f.format(17.6),f.format(orders.getPrice()));
    }

    @Test
    public void test_createNewOrder_with_cup() throws Exception { 

        Orders o2 = orderController.createNewOrder(10,dr1,c1,35);
        assertEquals(o2.getOrderedDrink(),dr1);
        assertEquals(o2.getOrderedCup(),c1);
        assertEquals(o2.getQuantity(),35);
        assertEquals(o2.getSugarQuantity(),10);
        assertEquals(o2.getPrice(),17.6);
    }

    @Test
    public void test_createNewOrder_with_cup_but_no_quantity() throws Exception {
        c1.setQuantity(0);
       
        assertThrows(NullPointerException.class, () -> {orderController.createNewOrder(10,dr1,c1,35);});

    }

    @Test
    public void test_createNewOrder_with_no_cup() throws Exception {
        Orders o2 = orderController.createNewOrder(10,dr1,null,35);
        assertEquals(o2.getOrderedDrink(),dr1);
        assertEquals(o2.getOrderedCup(),null);
        assertEquals(o2.getQuantity(),35);
        assertEquals(o2.getSugarQuantity(),10);
        assertEquals(o2.getPrice(),17.5);


    }

    @Test
    public void test_insertOrder_is_OK(){ 
        orders.setPrice(1000);
        orderController.insertOrder(orders);

        Query query = Hibernate.em.createQuery("FROM Orders o WHERE o.price =: price ");
        query.setParameter("price",1000);

        assertEquals(orders,query.getSingleResult());

    }

}
