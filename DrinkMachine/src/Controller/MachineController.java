package Controller;

import java.util.*;

import model.*;

import javax.persistence.Query;

import bddutil.Hibernate;

public class MachineController {

	private static Machine machine;

	public MachineController(int water,int sugar){
		machine = new Machine();
		machine.setSugarGr(sugar);
		machine.setWaterCl(water);
	}

	public MachineController(){

	}

	public boolean MachineInitialisazion(int id) {
		
		machine = Hibernate.em.find(Machine.class, id);
		
		if(machine == null || ControllerWaterQuantity() == 2) { 
			return false;
		}
		
		return true;
	}
	public Set<Drink> controllerDrinks(){
		
		return machine.getDrinksList();	
	}
	public Machine getMachine() 
	{
		return machine;
	}
	public List<Cup> getCupList(){
		Query query = Hibernate.em.createQuery("FROM Cup");
		return query.getResultList();
	}
	public Cup getSpecificCup(int formatCup){


		for(Cup cup : machine.getStockCup()) 
		{
			if(cup.getSize() == formatCup) 
			{
				return cup;
			}
		}
		return null;
	}
	public int ControllerWaterQuantity() {
		
		float water = machine.getWaterCl();
		if (water<35) {
			return 2;
		}

		else if(water < 75) {
			return 1;
		}
		return 0;
	}
	public int ControllerCupsQuantity(int cupFormat) 
	{
		for(Cup cupfor : machine.getStockCup()) 
		{
			if(cupfor.getSize() == cupFormat) 
			{
				return cupfor.getQuantity();
			}
		}
		return 0;
	}
	public int ControllerSugarQuantity() {
		
		int sugarquantityOfMachine = machine.getSugarGr();
		int value = 4;
		
		if(sugarquantityOfMachine < 5 ) {
			value =  0;
		}
		else if(sugarquantityOfMachine >= 25) {
			value = 1;
		}
		else if(sugarquantityOfMachine >= 20) {
			value = 2;
		}
		else if(sugarquantityOfMachine >= 15) {
			value = 3;
		}
		else if(sugarquantityOfMachine >= 10) {
			value = 4;
		}
		
		else if(sugarquantityOfMachine >= 5) {
			value = 5;
		}
		return value;
	}
	/**
	 * cette methode permet de mettre a  jour les quantites de consommables d'une machine a  partir d'une commande confirmee
	 */
	public void updateConsumablesMachine(Orders orderConfirmed) 
	{
		Hibernate.em.getTransaction().begin();
		
		machine.subtractSugar(orderConfirmed.getSugarQuantity());
		machine.subtractWater(orderConfirmed.getQuantity());
		machine.substractCup(orderConfirmed.getOrderedCup());
		Hibernate.em.getTransaction().commit();
	}
}