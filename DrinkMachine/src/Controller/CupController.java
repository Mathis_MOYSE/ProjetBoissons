package Controller;
import java.util.List;


import javax.persistence.Query;
import javax.persistence.TypedQuery;

import bddutil.Hibernate;
import model.*;

public class CupController {
	
public Cup getCupController(int idCup) {
		
	return Hibernate.em.find(Cup.class,idCup);
}
	
public boolean addCup(Cup Cup){
	Hibernate.em.getTransaction().begin();
	Hibernate.em.persist(Cup);
	Hibernate.em.getTransaction().commit();
		
	if (Hibernate.em.contains(Cup)) {
		return true;
	} 
		return false;
	}	
	
public List<Cup> getAllCups(){
		
	 Query query = Hibernate.em.createQuery("from cup"); 
		
     List<Cup> list = null;
			
	 if(query.getResultList().stream().findFirst().orElse(null)==null)
		return null;
	 else list = query.getResultList();
		return list;
	}
	
public Cup getCup(int idCup){
		
	String query = "from cup WHERE idCup = :ID";
		
	TypedQuery<Cup> tq = Hibernate.em.createQuery(query, Cup.class);
	tq.setParameter("ID", idCup);
	Cup Cup = null;
		
	if(tq.getResultList().stream().findFirst().orElse(null)==null)
		return null;
	else Cup = tq.getSingleResult();
		
	return Cup;
	
	}

public boolean ModifCup(Cup newCup){
	
	Hibernate.em.getTransaction().begin();
	Cup oldCup = Hibernate.em.find(Cup.class, newCup.getIdCup());
		
  if(oldCup != null)
  {
	oldCup.setMachineCup(newCup.getMachineCup());
	oldCup.setOrderedList(newCup.getOrderedList());
	oldCup.setPrice(newCup.getPrice());
	oldCup.setQuantity(newCup.getQuantity());
	oldCup.setTaille(newCup.getTaille());
	oldCup.setSize(newCup.getSize());
			
	Hibernate.em.persist(oldCup);
	Hibernate.em.getTransaction().commit();
	return true;
}
	return false;
}

public  boolean DeleteCup(int idCup){
    	
	Hibernate.em.getTransaction().begin();
	Cup Cup = Hibernate.em.find(Cup.class, idCup);

    if(Cup != null)
	{
    	Hibernate.em.remove(Cup);
    	Hibernate.em.persist(Cup);
    return true;
    }
    return false;
    }

	public int getNbAvailableCup(int cupFormat) 
	{
		Cup typeCupToCheck = Hibernate.em.createQuery("FROM Cup C WHERE C.Size =:size", Cup.class).setParameter("size", cupFormat).getSingleResult();
		
		if(typeCupToCheck != null) 
		{
			
		} 
		return typeCupToCheck.getQuantity();
	}
}
