package Controller;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import bddutil.Hibernate;
import model.*;

public class DrinkController {

public Drink getDrinkController(int idDrink) {
		
	return Hibernate.em.find(Drink.class,idDrink);
}
	
public boolean addDrink(Drink Drink){
	Hibernate.em.getTransaction().begin();
	Hibernate.em.persist(Drink);
	Hibernate.em.getTransaction().commit();
		
		if (Hibernate.em.contains(Drink)) {
			  return true;
		} 
		return false;
	}	
	
public List<Drink> getAllDrinks(){
		
	 Query query = Hibernate.em.createQuery("from Drink"); 
		
     List<Drink> list = null;
			
	 if(query.getResultList().stream().findFirst().orElse(null)==null)
		return null;
	 else list = query.getResultList();
		return list;
	}
	
public Drink getDrink(int idDrink){
		
	String query = "from Drink WHERE idBoisson = :ID";
		
	TypedQuery<Drink> tq = Hibernate.em.createQuery(query, Drink.class);
	tq.setParameter("ID", idDrink);
	Drink Drink = null;
		
	if(tq.getResultList().stream().findFirst().orElse(null)==null)
		return null;
	else Drink = tq.getSingleResult();
		
	return Drink;
	
	}


public boolean ModifDrink(Drink newDrink){
	
	Hibernate.em.getTransaction().begin();
	Drink oldDrink = Hibernate.em.find(Drink.class, newDrink.getIdBoisson());
		
  if(oldDrink != null)
  {
	oldDrink.setDescription(newDrink.getDescription());
	oldDrink.setMachineBelongList(newDrink.getMachineBelongList());
	oldDrink.setOrdersList(newDrink.getOrdersList());
	oldDrink.setPrixCl(newDrink.getPrixCl());
			
	Hibernate.em.persist(oldDrink);
	Hibernate.em.getTransaction().commit();
	return true;
}
	return false;
}
    
public  boolean DeleteDrink(int idDrink){
    	
	Hibernate.em.getTransaction().begin();
	Drink Drink = Hibernate.em.find(Drink.class, idDrink);

    if(Drink != null)
	{
    	Hibernate.em.remove(Drink);
    	Hibernate.em.persist(Drink);
    return true;
    }
    
    return false;
    }
}
