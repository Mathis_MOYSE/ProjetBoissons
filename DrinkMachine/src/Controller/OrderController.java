package Controller;


import java.text.DecimalFormat;
import java.time.LocalDateTime;

import bddutil.Hibernate;
import model.Cup;
import model.Drink;
import model.Orders;

public class OrderController {
	public void setPriceOrder(Orders newOrder) 
	{
		if(newOrder.getOrderedCup() != null) 
		{
			float price = newOrder.getOrderedCup().getPrice() + newOrder.getOrderedDrink().getPrixCl()*newOrder.getQuantity();
			newOrder.setPrice(price);
		}
		else 
		{
			float price = newOrder.getOrderedDrink().getPrixCl()*newOrder.getQuantity();
			newOrder.setPrice(price);
		}
	}
	public Orders createNewOrder(int quantitySugarOrder,Drink drinkOrder, Cup typeCupOrder, int sizeDrink) throws Exception 
	{
		Orders newOrder;
		if(typeCupOrder != null) 
		{
			CupController cupController = new CupController();
			int availableQuantityCups = cupController.getNbAvailableCup(typeCupOrder.getSize());
			if(availableQuantityCups > 0) 
			{
				float price = typeCupOrder.getPrice() + drinkOrder.getPrixCl()*typeCupOrder.getSize();
				newOrder = new Orders(quantitySugarOrder, drinkOrder, typeCupOrder, price, sizeDrink);
			}
			else 
			{
				Exception e = new Exception("nombre de gobelets insuffisant");	
				throw e;
			}
		}
		else 
		{
			float price = drinkOrder.getPrixCl()*sizeDrink;
			newOrder = new Orders(quantitySugarOrder, drinkOrder, null,price,sizeDrink);
			
		}
		return newOrder;
	}
	public void insertOrder(Orders orderToPush)
	{
		Hibernate.em.getTransaction().begin();
		orderToPush.setDateTime(LocalDateTime.now());
		setPriceOrder(orderToPush);
		Hibernate.em.persist(orderToPush);
		Hibernate.em.getTransaction().commit();
	}

}
