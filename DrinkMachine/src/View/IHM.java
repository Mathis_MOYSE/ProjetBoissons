package View;

import java.util.*;

import Controller.*;
import model.*;

public class IHM {
	
	//attributes : 
	
	private MachineController machineController = new MachineController();	
	private OrderController orderController = new OrderController();
	private DrinkController drinkController = new DrinkController();
	private CupController cupController = new CupController();
	
	public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        } catch(NullPointerException e) {
            return false;
        }
        return true;
    }
	
	public boolean GeneralMenu() {
		
		int choice = -2;
        String inputChoice;
        Scanner sc = new Scanner(System.in);
        
        boolean OutOfService = machineController.MachineInitialisazion(1);
        
        if(OutOfService == false) {
        	System.out.println("Sorry but this machine is out of service");
        	return true;
        }
        
        System.out.println("Welcome! The Drink Machine is here to serve you");
        System.out.println("Entry 1 for begin a Order!!");

        while(choice != 1){
            do
            {
                inputChoice = sc.next();
                
                // call isInteger function for test the type of variable
                if(isInteger(inputChoice) == false) System.out.println("Please enter a integer Number!");

            }while(isInteger(inputChoice) == false);

            choice = Integer.parseInt(inputChoice);

            if(choice !=1){
                System.out.println("Please input a valid number! Or are you too drunk for that ?");
            }
        }

        if(choice == 1){
             MenuOrder();
        }

        else{
            System.exit(0);
        }

        return false;
	}
	
	public void MenuOrder() {
		
		int idDrink;
		int CupChoice;
		int SugarQuantity;
		int DrinkQuantity;
		
		System.out.println("Amazing! Let's begin!");
		System.out.println("Getting started to choice Drink!");
		System.out.print("For that entry the matching number that can see! \n");
		Orders newOrder = new Orders();
		newOrder.setOrderedMachine(machineController.getMachine());

		idDrink = ChoiceDrink(newOrder);
		
		DrinkQuantity = ChoiceQuantity(newOrder);
		
		CupChoice = ChoiceCup(newOrder);
		
		SugarQuantity = choiceSugarQuantity(newOrder);
		
		Summary(idDrink,CupChoice,SugarQuantity,DrinkQuantity);
			
		OrderConfimation(newOrder);
	}
	
	public Set<Integer> ShowDrinks() {
		Set<Drink> listDrink =machineController.controllerDrinks();

		ArrayList<Drink> listDrink2 = new ArrayList<>();
		listDrink2.addAll(listDrink);

		Collections.sort(listDrink2,new DrinkComparator());

		Set<Integer> listIdDrink = new HashSet<Integer>();
		
		for (Drink drink : listDrink2) {
			System.out.println(drink.getIdBoisson()+" : "+ drink.getNom()+" ");
			System.out.println("Description : "+drink.getDescription());
			listIdDrink.add(drink.getIdBoisson());
		}
		
		return listIdDrink;
	}
	
	public void ShowCup() {
		
		
	}

	public int ChoiceDrink(Orders order) {
		int choice = -2;
        String inputChoice;
        Scanner sc = new Scanner(System.in);
        
        Set<Integer> listIdDrink = ShowDrinks();
        
        choice = Choice(listIdDrink);
        order.setOrderedDrink(drinkController.getDrink(choice));
		return choice;
		
	}
	
	public int ChoiceCup(Orders order){
		int choice = -2;
        String inputChoice;
        Scanner sc = new Scanner(System.in);
        
        Set<Integer> choicelist = new HashSet<Integer>();
		choicelist.add(1);
		choicelist.add(2);
        
		System.out.println("Do you want a cup with your order ? ");
		System.out.println("Entry 1 for yes");
		System.out.println("Entry 2 for no");
		
		choice = Choice(choicelist);
		if(choice == 1 ) 
		{
			Cup cupOrdered = machineController.getSpecificCup(order.getQuantity());
			order.setOrderedCup(cupOrdered);
		}
		else 
		{
			order.setOrderedCup(null);
		}
		return choice;	
	}
	
	public int choiceSugarQuantity(Orders orderToModify) {
		int choice = 0;
        String inputChoice;
        Scanner sc = new Scanner(System.in);
        Set<Integer> choicelist;
        //valeur controller représente la quantité de sucre restante dans la machine
        int valuecontroller = machineController.ControllerSugarQuantity();
       
		if(valuecontroller == 1) {
			choicelist = new HashSet<Integer>();
	        choicelist.add(0);
	        choicelist.add(1);
			choicelist.add(2);
			choicelist.add(3);
			choicelist.add(4);
			choicelist.add(5);
			
			System.out.println("Choice a Sugar quantity between 0 and 5");
			System.out.println("Each landing increase of 5gr of Sugar");
			
			choice = Choice(choicelist);
		}
		
		else if(valuecontroller == 2) {
			choicelist = new HashSet<Integer>();
	        choicelist.add(0);
	        choicelist.add(1);
			choicelist.add(2);
			choicelist.add(3);
			choicelist.add(4);
			
			System.out.println("/!\\ Not nough sugar for all choice!!");
			System.out.println("Choice a Sugar quantity between 0 and 4");
			System.out.println("Each landing increase of 5gr of Sugar");
			
			choice = Choice(choicelist);
		}
		
		else if(valuecontroller == 2) {
			choicelist = new HashSet<Integer>();
	        choicelist.add(0);
	        choicelist.add(1);
			choicelist.add(2);
			choicelist.add(3);
			
			System.out.println("/!\\ Not nough sugar for all choice!!");
			System.out.println("Choice a Sugar quantity between 0 and 4");
			System.out.println("Each landing increase of 5gr of Sugar");
			
			choice = Choice(choicelist);
		}
		
		else if(valuecontroller == 3) {
			choicelist = new HashSet<Integer>();
	        choicelist.add(0);
	        choicelist.add(1);
			choicelist.add(2);
			choicelist.add(3);
			
			System.out.println("/!\\ Not Enough sugar for all choice!!");
			System.out.println("Choice a Sugar quantity between 0 and 3");
			System.out.println("Each landing increase of 5gr of Sugar");
			
			choice = Choice(choicelist);
		}
		
		else if(valuecontroller == 4) {
			choicelist = new HashSet<Integer>();
	        choicelist.add(0);
	        choicelist.add(1);
			choicelist.add(2);
			
			System.out.println("/!\\ Not Enough sugar for all choice!!");
			System.out.println("Choice a Sugar quantity between 0 and 2");
			System.out.println("Each landing increase of 5gr of Sugar");
			
			choice = Choice(choicelist);
		}
		
		else if(valuecontroller == 5) {
			choicelist = new HashSet<Integer>();
	        choicelist.add(0);
	        choicelist.add(1);
			
	        System.out.println("/!\\ Not Enough sugar for all choice!!");
			System.out.println("Choice a Sugar quantity between 0 or 1");
			System.out.println("Each landing increase of 5gr of Sugar");
			
			choice = Choice(choicelist);
		}
		
		else if(valuecontroller == 5) {
			choice = 0;
			System.out.println("There is no more sugar!!!");
			
			if(OrderConfimation(null) == false) {
				System.out.println("GoodBye!!!");
				System.exit(0);
			}
		}
		orderToModify.setSugarQuantity(choice*5);
		return choice;		
	}
	
	public int ChoiceQuantity(Orders order) {
		int quantityChoice = -1;
		
		Set<Integer> choicelist;
		
		choicelist = new HashSet<Integer>();
		
		if(machineController.ControllerWaterQuantity() == 0 && machineController.ControllerCupsQuantity(quantityChoice) >= 0)
		{
			choicelist.add(35);
			choicelist.add(75);

			System.out.println("Here is the quantity that can choose : ");
			System.out.println("Entry 35 for choose 35cl");
			System.out.println("Entry 75 for choose 75cl");

			quantityChoice = Choice(choicelist);
			
		}
		else {
			
			System.out.println("Not enought water quantity for a 75cl drink");
			System.out.println("Default choice is 35 cl for your drink");
	
			quantityChoice = 35;
		}
		order.setQuantity(quantityChoice);
		return quantityChoice;	
	}
	
	
	public int Choice(Set<Integer> list) {
		int choice = -1;
        String inputChoice;
        Scanner sc = new Scanner(System.in);
        
        while(list.contains(choice) == false){
            do
            {
                inputChoice = sc.next();
                
                // call isInteger function for test the type of variable
                
                if(isInteger(inputChoice) == false) System.out.println("Please enter a integer Number!");

            }while(isInteger(inputChoice) == false);

            choice = Integer.parseInt(inputChoice);

            if(list.contains(choice) == false){
                System.out.println("Please input a valid number!");
            }
        }
        
        return choice;
	}
	public void Summary(int idDrink,int choiceCup,int SugarQuantity,int DrinkQuantity) {
		Drink drink = drinkController.getDrinkController(idDrink);
		System.out.println("Here is your order Summary : ");
		System.out.println("Drink : "+drink.getNom());
		System.out.println("Quantity : "+ DrinkQuantity +" cl");	
		if(choiceCup == 1) {
			System.out.println("Cup : Yes ");
		}
		else {
			System.out.println("Cup : No ");
		}
		System.out.println("Sugar Quantity : "+SugarQuantity*5 + " gr");	
	}
	public boolean OrderConfimation(Orders orderToConfirm) {
		int confirmation;
		System.out.println("Do you want confirm this order ? ");
		System.out.println("Entry 1 for Yes");
		System.out.println("Entry 2 for No ");
		Set<Integer> choicelist = new HashSet<Integer>();
		choicelist.add(1);
		choicelist.add(2);
		confirmation = Choice(choicelist);
		if(confirmation == 1) {
			orderToConfirm.setStatus(true);
			orderController.insertOrder(orderToConfirm);
			machineController.updateConsumablesMachine(orderToConfirm);
			return true;
		}
		else {
			orderToConfirm.setStatus(false);
			orderController.insertOrder(orderToConfirm);
			return false;
		}
	}
}