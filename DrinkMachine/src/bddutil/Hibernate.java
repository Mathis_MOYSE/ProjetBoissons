package bddutil;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Hibernate {
    private static EntityManagerFactory entityManagerFactory;
    public  static EntityManager em;

    public static void init(){
        entityManagerFactory = Persistence.createEntityManagerFactory("test");
        em = entityManagerFactory.createEntityManager();
    }

    public Hibernate(String name ){
        Persistence.createEntityManagerFactory(name);
        em = entityManagerFactory.createEntityManager();
    }
    
    public static void close() {
		em.close();
		entityManagerFactory.close();		
	}
    

}
