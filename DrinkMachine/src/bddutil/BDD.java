package bddutil;

import java.time.LocalDateTime;
import model.*;

public class BDD {

	public void JeuxDeDonnees() {
		
		Cup cup1 = new Cup();
		cup1.setPrice((float)0.35);
		cup1.setQuantity(100);
		cup1.setSize(35);
		
		Cup cup2 = new Cup();
		cup2.setPrice((float)0.55);
		cup2.setQuantity(100);
		cup2.setSize(75);
		
		Drink dr1 = new Drink();
		dr1.setNom("Coca-Cola");
		dr1.setDescription("Boisson Sucree");
		dr1.setPrixCl((float)0.75);
		
		Drink dr2 = new Drink();
		dr2.setNom("Pepsi");
		dr2.setDescription("Copie du cola ");
		dr2.setPrixCl((float)0.55);
		
		Drink dr3 = new Drink();
		dr3.setNom("Ice Tea");
		dr3.setDescription("The glace tres connu");
		dr3.setPrixCl((float)0.40);
		
		Machine machine1 = new Machine();
		machine1.setIdMachine(1);
		machine1.setWaterCl(1000);
		machine1.setSugarGr(1000);
		machine1.addDrink(dr1);
		machine1.addDrink(dr2);
		machine1.addDrink(dr3);
		machine1.addCup(cup1);
		machine1.addCup(cup2);
		
		Orders order = new Orders();
		order.setOrderedMachine(machine1);
		order.setOrderedDrink(dr3);
		order.setOrderedCup(cup2);
		order.setDateTime(LocalDateTime.now().plusDays(1));
		order.setQuantity(75);
		order.setStatus(true);
		order.CalculatePrice();
		
		Orders order1 = new Orders();
		order1.setOrderedMachine(machine1);
		order1.setOrderedDrink(dr1);
		order1.setDateTime(LocalDateTime.now().plusDays(1));
		order1.setQuantity(35);
		order1.setStatus(true);
		order1.CalculatePrice();
		

		
		Hibernate.em.getTransaction().begin();
		
		Hibernate.em.persist(cup1);
		
		Hibernate.em.persist(cup2);
		Hibernate.em.persist(dr1);
		Hibernate.em.persist(dr2);
		Hibernate.em.persist(dr3);
		Hibernate.em.persist(machine1);
		Hibernate.em.persist(order);
		Hibernate.em.persist(order1);
		
		
		Hibernate.em.getTransaction().commit();
		
		
		
	}
}
