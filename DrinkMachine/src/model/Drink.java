package model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;


@Entity
public class Drink {
	//attributs :  
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idBoisson;
	private String name;
	private String description;
	private float priceCl;
	
	@OneToMany(mappedBy = "orderedDrink",cascade = CascadeType.ALL)
	private Set<Orders> ordersList;
	
	@ManyToMany
	@JoinTable(name="BelongMachineDrink",
            joinColumns=@JoinColumn(name="idBoisson"),
            inverseJoinColumns=@JoinColumn(name="idMachine"))
	private Set<Machine> machineBelongList;

		//contructor : 
	
	public Drink() {
		name = "";
		description = "";
		priceCl = 0;
		ordersList = new HashSet<Orders>();
		machineBelongList = new HashSet<Machine>();
		
	}
	
		//methods : 
	
	public int getIdBoisson() {
		return idBoisson;
	}

	public void setIdBoisson(int idBoisson) {
		this.idBoisson = idBoisson;
	}

	public String getNom() {
		return name;
	}

	public void setNom(String nom) {
		name = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getPrixCl() {
		return priceCl;
	}
	
	
	public void setPrixCl(float prixCl) {
		this.priceCl = prixCl;
	}
	
	public Set<Orders> getOrdersList() {
		return ordersList;
	}

	public void setOrdersList(Set<Orders> ordersList) {
		this.ordersList = ordersList;
	}

	public Set<Machine> getMachineBelongList() {
		return machineBelongList;
	}

	public void setMachineBelongList(Set<Machine> machineBelongList) {
		this.machineBelongList = machineBelongList;
	}

	public void addMachine( Machine machine) {
		if(machine != null)
			machineBelongList.add(machine);
	}
	
	public void removeMachine(Machine machine) {
		if(machine != null)
			machineBelongList.remove(machine);
	}
	
	public void addOrder(Orders order) {
		if(order != null) 
			ordersList.add(order);
	}
	
	public void removeOrder(Orders order) {
		if(order != null) 
			ordersList.remove(order);
	}

}
