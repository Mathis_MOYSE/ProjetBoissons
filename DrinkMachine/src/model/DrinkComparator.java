package model;

import java.util.Comparator;

public class DrinkComparator implements Comparator<Drink> {

    @Override
    public int compare(Drink dr1, Drink dr2) {

        if(dr1.getIdBoisson() < dr2.getIdBoisson())
            return -1;
        else
            return 1;

    }
}
