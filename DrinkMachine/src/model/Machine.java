package model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;


@Entity
public class Machine {
		//attributs :
	@Id
	private int idMachine;
	private int WaterCl;
	private int SugarGr;
	
	@OneToMany(mappedBy = "orderedMachine",cascade = CascadeType.ALL)
	private Set<Orders> orderedList;
	
	@ManyToMany
	private Set<Drink> drinksList;
	
	@OneToMany
	private Set<Cup> stockCup;
	
	
	public Machine() {
		WaterCl = 0;
		SugarGr = 0;
		orderedList = new HashSet<>();
		drinksList = new HashSet<>();
		stockCup = new HashSet<>();
	}
	
		//methods : 
	
	public int getIdMachine() {
		return idMachine;
	}

	public void setIdMachine(int idMachine) {
		this.idMachine = idMachine;
	}

	
	public int getWaterCl() {
		return WaterCl;
	}

	public void setWaterCl(int waterCl) {
		WaterCl = waterCl;
	}

	public int getSugarGr() {
		return SugarGr;
	}

	public void setSugarGr(int sugarGr) {
		SugarGr = sugarGr;
	}

	public Set<Orders> getOrderedList() {
		return orderedList;
	}

	public void setOrderedList(Set<Orders> orderedList) {
		this.orderedList = orderedList;
	}

	public Set<Drink> getDrinksList() {
		return drinksList;
	}

	public void setDrinksList(Set<Drink> drinksList) {
		this.drinksList = drinksList;
	}

	public Set<Cup> getStockCup() {
		return stockCup;
	}

	public void setStockCup(Set<Cup> stockCup) {
		this.stockCup = stockCup;
	}

	public void addOrder(Orders order) {
		if(order != null)
			orderedList.add(order);
	}
	
	public void removeOrder(Orders order) {
		if(order != null)
			orderedList.remove(order);
	}
	
	public void addDrink(Drink drink) {
		if(drink !=null) {
			drink.addMachine(this);
			drinksList.add(drink);
		}
	}
	
	public void removeDrink(Drink drink) {
		if(drink != null)
			drinksList.remove(drink);
	}
	
	public void addCup(Cup cup) {
		if(cup != null) {
			cup.setMachineCup(this);
			stockCup.add(cup);
		}
	}
	
	public void removeCup(Cup cup) {
		if(cup != null)
			stockCup.remove(cup);
	}	
	
	public void subtractWater(int WaterValue) {
		if(WaterCl > WaterValue) {
			WaterCl-=WaterValue;
		}
	}
	
	public void subtractSugar(int inputVal) 
	{
		if(getSugarGr() > inputVal) 
		{
			int currentQuantitySugar = getSugarGr();
			int newVal = currentQuantitySugar - inputVal;
			setSugarGr(newVal);			
		}
		
	}
	public void substractCup(Cup cupOrder) 
	{
		if(cupOrder != null) {
			cupOrder.TakeCup();
		}
	}
}
