package model;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Orders {
	
	//attributs :
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idC;
	private int Quantity;
	private boolean Status;
	private float price;
	private LocalDateTime DateTime;
	private int sugarQuantity;

	@ManyToOne(cascade = CascadeType.ALL)
	private Drink orderedDrink;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private Machine orderedMachine;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private Cup orderedCup;
	
	//constructor : 
	
	public Orders() {
	}
	public Orders(int quantitySugarOrder, Drink drinkOrder,Cup typeCupOrder, float priceOrder, int size) 
	{
		this.sugarQuantity = quantitySugarOrder;
		this.orderedDrink = drinkOrder;
		this.orderedCup = typeCupOrder;
		this.price = priceOrder;
		this.Quantity = size;
		this.DateTime = LocalDateTime.now();
	}
	
	//methods : 
	
	public int getIdC() {
		return idC;
	}
	
	public void setIdC(int idC) {
		this.idC = idC;
	}

	public LocalDateTime getDateTime() {
		return DateTime;
	}
	public int getSugarQuantity() {
		return sugarQuantity;
	}
	public void setSugarQuantity(int sugarQuantity) {
		this.sugarQuantity = sugarQuantity;
	}
	public void setDateTime(LocalDateTime dateTime) {
		DateTime = dateTime;
	}

	public int getQuantity() {
		return Quantity;
	}

	public void setQuantity(int quantity) {
		Quantity = quantity;
	}

	public boolean isStatus() {
		return Status;
	}

	public void setStatus(boolean status) {
		Status = status;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	public Drink getOrderedDrink() {
		return orderedDrink;
	}

	public void setOrderedDrink(Drink orderedDrink) {
		orderedDrink.addOrder(this);
		this.orderedDrink = orderedDrink;
	}
	
	public Machine getOrderedMachine() {
		return orderedMachine;
	}

	public void setOrderedMachine(Machine orderedMachine) {
		orderedMachine.addOrder(this);
		this.orderedMachine = orderedMachine;
	}

	public Cup getOrderedCup() {
		return orderedCup;
	}

	public void setOrderedCup(Cup orderedCup) {
		// non orderedCup.addOrder(this);
		this.orderedCup = orderedCup;
	}

	public void CalculatePrice() {
		if(orderedCup != null) {
			price = orderedCup.getPrice()+ Quantity*orderedDrink.getPrixCl();
		}
		else {
			price = Quantity*orderedDrink.getPrixCl();
		}
	}
	
}
