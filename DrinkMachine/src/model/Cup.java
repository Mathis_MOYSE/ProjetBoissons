package model;

import java.util.*;

import javax.management.RuntimeErrorException;
import javax.persistence.*;

/**
 * 
 * l'entite Cup represente un type de gobelet et non pas un objet cup individuel
 * un type de gobelet peut appartenir e  plusieurs machines, plusieurs machines peuvent partager un type de gobelet
 */
@Entity
public class Cup {
		//attributs :
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idCup;
	
	private int Quantity;
	private int Size;  
	private float Price;
	
	@OneToMany(mappedBy = "orderedCup")
	private Set<Orders> orderedList;
	@ManyToOne
	private Machine machineCup;
	

		//constructor : 
	public Cup() {
		Quantity = 0;
		Size = 0;
		Price = 0;
		orderedList = new HashSet<Orders>();
		machineCup = new Machine();
	}
	
	public Cup(int qte, int size, float price)
	{
		if (qte >=0)
		{
			Quantity = qte;
		}
		else
		{
			Quantity = 0;
		}
		Size = size;
		Price = price;
		orderedList = new HashSet<Orders>();
		machineCup = new Machine();
	}
	
		//methods : 
	
	public int getIdCup() {
		return idCup;
	}

	public void setIdCup(int idCup) {
		this.idCup = idCup;
	}

	public int getQuantity() {
		return Quantity;
	}

	public void setQuantity(int quantity) {
		this.Quantity = quantity;
	}

	public int getTaille() {
		return Size;
	}

	public void setTaille(int taille) {
		Size = taille;
	}
	
	public int getSize() {
		return Size;
	}

	public void setSize(int size) {
		Size = size;
	}

	public float getPrice() {
		return Price;
	}

	public void setPrice(float price) {
		Price = price;
	}

	public Set<Orders> getOrderedList() {
		return orderedList;
	}

	public void setOrderedList(Set<Orders> orderedList) {
		this.orderedList = orderedList;
	}

	
	public void TakeCup() {
		if (this.Quantity > 0) {
			this.Quantity--;
		}
			else
		{
			throw new RuntimeException("Il n'y a plus de gobelets disponible !");
		}
	}
	public void addOrder(Orders order) {
		if(order != null) 
			orderedList.add(order);
	}
	
	public void removeOrder(Orders order) {
		if(order != null) 
			orderedList.remove(order);
	}
	public Machine getMachineCup() {
		return machineCup;
	}

	public void setMachineCup(Machine machineCup) {
		this.machineCup = machineCup;
	}

}
