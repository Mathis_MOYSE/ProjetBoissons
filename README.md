# DrunkMachine
DrunkMachine est une application permettant la vente de boissons via un distributeur automatique.
## Installation
Pour pouvoir lancer l’application, il vous faudra au préalable avoir téléchargé Hibernate, le framework Java que nous avons utilisé pour lier l’application à une base de données. Il vous faudra également le connector J 8.0.
Installer un serveur SQL.
## Guide d’utilisation 
Lors du premier lancement du programme “Main.java”, pensez à décommenter ces lignes ci ce n’est pas déjà le cas:
```java
	BDD bdd = new BDD();
bdd.JeuxDeDonnees();
```
Recommentez-les pour les prochains lancements.
 Avant de l'exécuter, pensez à modifier votre fichier persistence.xml présent dans : META-INF.
Modifiez les lignes suivantes : 

Indiquez le nom de votre base de données à la place de “nomBDD” :
```xml
<property name="javax.persistence.jdbc.url" value="jdbc:mysql://localhost:3306/nomBDD"/>
```
N’oubliez pas de changer vos identifiants mySQL aux emplacements “root” :
```xml
<property name="javax.persistence.jdbc.user" value="root"/>
<property name="javax.persistence.jdbc.password" value="root"/>
```
La dernière chose à faire est de décommenter cette ligne si cela n’est pas déjà le cas : 
```xml
<!-- <property name="hibernate.hbm2ddl.auto" value="create"/> -->
```
Vous pouvez maintenant créer les tables.
Une fois la création terminée, recommentez la ligne que vous avez décommenté au-dessus.
## Contribution
Vous pourrez retrouver le projet en open source à ce lien :
[GIT](https://gitlab.com/Mathis_MOYSE/ProjetBoissons)


